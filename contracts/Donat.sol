pragma solidity ^0.8.0;

contract Donat{
    address public owner;
    mapping (address => uint) public payments;
    address public donatAddress;
    address[] private donaters;
 
    modifier onlyOwner {
        require(msg.sender == owner, "you are not owner");
        _;
    }

    constructor(){
        owner = msg.sender;
        donatAddress = address(this);
    }
    

    function transfer(address receiver, uint amount) external onlyOwner {
        address payable _to = payable(receiver);
        require(donatAddress.balance >= amount, "amount value is higher than the actual balance of contract");
        _to.transfer(amount);
    }

    function payDonat() external payable {
        if(payments[msg.sender] == 0) donaters.push(msg.sender); 
        payments[msg.sender] += msg.value;
    }


    
    function getDonatAmount(address donater) external view returns(uint) {
        return donater.balance;
    }

    function getDonatersList() external view returns(address[] memory) {
        return donaters;
    }
}