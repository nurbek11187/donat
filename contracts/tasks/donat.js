// 0x5FbDB2315678afecb367f032d93F642f64180aa3
const hre = require("hardhat");

const ethers = hre.ethers
const donatartefact = require('../artifacts/contracts/Donat.sol/Donat.json')
console.log(donatartefact, typeof(donatartefact))


async function main(){
    const [signer] = await ethers.getSigners()

    const  donat_address = '0x5FbDB2315678afecb367f032d93F642f64180aa3'
    const donatContract = new ethers.Contract(
        donat_address,
        donatartefact.abi,
        signer
    )
    //TASKS
    const setresult  = await donatContract.getDonatAmount("0x5B6b6b9bDC018E1b21693D51DDFc57cC7625769c")    
    const result = await donatContract.getDonatersList()
    const paydonat = await donatContract.payDonat()
    const transfer = await donatContract.transfer("0x5B6b6b9bDC018E1b21693D51DDFc57cC7625769c", 1000)

    //console.log(result)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  })